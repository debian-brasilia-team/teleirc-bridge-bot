# teleirc-bridge-bot

Quick start guide to using TeleIRC.

### Create bot with BotFather

BotFather is the Telegram bot for [creating Telegram bots](https://core.telegram.org/bots#6-botfather). [See the official Telegram documentation for how to create a new bot](https://core.telegram.org/bots#creating-a-new-bot).

Once you create a new bot, you must follow these additional steps **(IN EXACT ORDER)**:
1. Send /setprivacy to `@BotFather`, change to Disable why?
2. Add bot to Telegram group to be bridged
3. Send /setjoingroups to `@BotFather`, change to Disable why?

### Configure IRC channel

No matter what IRC network you use, TeleIRC developers recommend this IRC channel configuration:

- Register your IRC channel with the IRC network (i.e. ChanServ).
- Unauthenticated users may join the channel.
- Only authenticated users may write in the channel (i.e. NickServ).
- Any user connecting from a network-recognized gateway (e.g. web chat) with an assigned hostmask automatically receives voice on join (and thus, does not need to authenticate to write in channel).
- TeleIRC bot hostmask automatically receives voice on join (and thus, does not need to authenticate to write in channel).
- IRC channel operators automatically receive operator privilege on join.

### Environment  variables

TeleIRC uses [godotenv](https://github.com/joho/godotenv) to manage API keys and settings. The config file is a `.env` file. Copy the example file to a production file to get started (`cp env.example .env`). Edit the `.env` file with your API keys and settings.

See [Config file glossary](https://docs.teleirc.com/en/latest/user/config-file-glossary/) for detailed information.

Telegram chat ID of bridged group ([how do I get this?](https://docs.teleirc.com/en/latest/user/faq/#chat-id))

1. Add the Telegram BOT to the group.

2. Get the list of updates for your BOT:
```bash
https://api.telegram.org/bot<YourBOTToken>/getUpdates
# Ex:
https://api.telegram.org/bot123456789:jbd78sadvbdy63d37gda37bd8/getUpdates
```

3. Look for the "chat" object:
```json
{"update_id":8393,"message":{"message_id":3,"from":{"id":7474,"first_name":"AAA"},"chat":{"id":,"title":""},"date":25497,"new_chat_participant":{"id":71,"first_name":"NAME","username":"YOUR_BOT_NAME"}}}
```

> This is a sample of the response when you add your BOT into a group.

4. Use the `"id"` of the `"chat"` object to send your messages.


### Deployment Guide with Docker

```bash
docker run -d --name teleirc --restart always \
    --env-file .env \
    debianbrasilia/teleirc-bridge-bot:2.1.0

# or
docker run -d --name teleirc --restart always \
    -e IRC_SERVER=irc.oftc.net \
    -e TELEIRC_TOKEN="000000000:AAAAAAaAAa2AaAAaoAAAA-a_aaAAaAaaaAA" \
    -e IRC_CHANNEL="#debian-bsb" \
    -e IRC_BOT_NAME="debian-brasilia-bridge-bot" \
    -e TELEGRAM_CHAT_ID="-0000000000000" \
    -e IRC_BOT_REALNAME="Debian Brasilia IRC Bridge" \
    -e IRC_BOT_IDENT="debian_brasilia_bridge_bot" \
    debianbrasilia/teleirc-bridge-bot:2.1.0
```